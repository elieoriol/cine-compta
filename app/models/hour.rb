class Hour < ApplicationRecord
  belongs_to :day, optional: false

  def maj
    maj_rate + sup_rate + night_rate + ant_rate
  end

  def final_maj
    maj <= 100 ? maj : 100
  end

  def final_night_rate
    return night_rate if maj <= 100
    maj - 100 <= night_rate ? maj - 100 : night_rate
  end

  def final_maj_rate
    return maj_rate if maj <= 100
    maj - final_night_rate - 100
  end

  def final_ant_rate
    return ant_rate if maj <= 100
    maj - 100
  end


  # Calculate rate from sup hour
  def sup_rate
    return 0 if week_num <= 35
    return 25 if week_num <= 43
    return 50 if week_num <= 48
    75
  end

  # Calculate rate from night hour
  def night_rate
    return 50 if day_hour < 6
    return 50 if day_hour >= 22 && day_hour < 28 && period == 1
    return 50 if day_hour >= 20 && day_hour < 28 && period == 2
    0
  end

  # Calculate rate from maj hour
  def maj_rate
    return 100 if shooting_hour_num > 10
    0
  end

  # Calculate rate from anticipated hour
  def ant_rate
    previous_day = day.shooting.days.where(date: day.date - 1.days).take
    return 0 if previous_day.nil?

    last_hour = previous_day.end_hour
    return 100 if (last_hour + 11) - (day_hour + 24) >= 0
    0
  end

  # Probably have to correct this to
  def shooting_hour_num
    shooting_hours = day.week.shooting_hours[day.date.cwday.to_s]
    return 0 if shooting_hours['start'].nil? || day_hour < shooting_hours['start'].to_f
    day_hour - shooting_hours['end'].to_f
  end

  def period
    year = day.date.strftime('%Y').to_i
    date_range = Date.new(year, 4, 1)..Date.new(year, 9, 30)

    return 1 if date_range.cover?(day.date)
    2
  end
end
