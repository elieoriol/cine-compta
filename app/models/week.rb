class Week < ApplicationRecord
  belongs_to :shooting, optional: false

  has_many :days, dependent: :destroy

  def week_number
    Week.where("beginning_date <= ?", self.beginning_date).count
  end
end
