class Job < ApplicationRecord
  has_many :employees

  validates_presence_of :name
  validates_presence_of :base_salary

  def salary_per_hour
    (base_salary/39.to_f).round(2)
  end

  def thirty_five_salary
    (salary_per_hour*35.to_f).round(2)
  end

  def equiv_diff
    equiv_from - equiv_to
  end

  def day_1_5
    (base_salary/5.0).round(2)
  end

  def day_extra
    (salary_per_hour*7.0*1.25).round(2)
  end

  def half_hour
    (salary_per_hour*0.5).round(2)
  end

  def hour_125
    (salary_per_hour*1.25).round(2)
  end

  def hour_150
    (salary_per_hour*1.5).round(2)
  end

  def hour_175
    (salary_per_hour*1.75).round(2)
  end

  def hour_200
    (salary_per_hour*2.0).round(2)
  end

  def maj_25
    (salary_per_hour*0.25).round(2)
  end

  def maj_50
    (salary_per_hour*0.5).round(2)
  end
end
