class Day < ApplicationRecord
  belongs_to :week, optional: false
  belongs_to :shooting
  belongs_to :employee, optional: false

  has_many :hours, dependent: :destroy

  validates_presence_of :date

  def start_date
    date_from_hour(start_hour)
  end

  def end_date
    date_from_hour(end_hour)
  end

  def lunch_start_date
    date_from_hour(lunch_hour)
  end

  def lunch_end_date
    date_from_hour(lunch_hour + 1.0)
  end

  def hours_number
    end_hour - start_hour - 1.0
  end

  def get_week_hours_number
    week_day_number = date.cwday
    week_hours_number = 0
    1.upto(week_day_number-1) do |i|
      day = shooting.days.where(date: date - i.days).take
      week_hours_number += day.hours_number unless day.nil?
    end
    week_hours_number
  end

  def create_hours
    base_week_num = get_week_hours_number

    1.upto(end_hour-start_hour) do |i|
      hour = start_hour + i - 1
      next if hour == lunch_hour

      hours.create(day_hour: hour, week_num: base_week_num + i, day_num: i)
    end
  end

  def hours_counts_hash
    count_hash = {
        '100': 0,
        '125': 0,
        '150': 0,
        '175': 0,
        '200': 0,
    }
    hours.each do |h|
      count_hash[:"#{(h.final_maj + 100).to_s}"] += 1
    end
    count_hash
  end

  def merge_hours_counts_hash(hash)
    count_hash = hours_counts_hash

    return count_hash if hash.empty?

    count_hash.keys.each { |k| count_hash[k] += hash[k] }
    count_hash
  end


  def date_from_hour(hour)
    real_day = date.day
    real_hour = hour
    if hour >= 24.0
      real_day += 1
      real_hour -= 24
    end
    DateTime.new(date.year, date.month, real_day, real_hour)
  end

  def as_json(options = {})
    if options[:lunch]
      json = lunch_hour.nil? ? nil : {
          id: id + '-lunch',
          start: lunch_start_date.to_s[0..-7],
          end: lunch_end_date.to_s[0..-7],
          title: 'Lunch'
      }
    else
      json = {
          id: id,
          start: start_date.to_s[0..-7],
          end: end_date.to_s[0..-7],
          title: ''
      }
    end

    json
  end
end
