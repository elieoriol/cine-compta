class Indemnity < ApplicationRecord
  belongs_to :shooting

  validates_presence_of :name
  validates_presence_of :value
end
