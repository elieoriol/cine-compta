class Employee < ApplicationRecord
  belongs_to :job, optional: false
  has_many :days, dependent: :destroy
  has_many :hours, through: :days
  belongs_to :shooting, optional: false

  validates_presence_of :name

  def week_hours(week_date)
    tot_week_hours = 0
    0.upto(6) do |i|
      day = days.where(date: week_date + i.days).take
      tot_week_hours += day.hours_number
    end
    tot_week_hours
  end

  def week_equiv_hours(week_date)
    tot_week_hours = week_hours(week_date)
    tot_week_hours - job.equiv_diff
  end

  def calculate_week_salary(week_date)
    hour_salary = job.salary_per_hour

    equiv_hours = week_equiv_hours(week_date)
    salary = 0
    0.upto(6) do |i|
      day = days.where(date: week_date + i.days).take

      day.hours.each do |h|
        salary += h.final_maj_rate*hour_salary
        equiv_hours -= 1
        break if (equiv_hours == 0)
      end

      break if (equiv_hours == 0)
    end
    salary
  end


  def ended_contract?
    !contract_end.nil?
  end
end
