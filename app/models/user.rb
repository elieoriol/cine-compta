class User < ApplicationRecord
  has_many :shootings

  has_secure_password

  validates :email, uniqueness: true
  validate :email_must_be_valid


  def self.login email, password
    user = User.find_by_email email
    if user
      if user.authenticate password
        return user
      else
        raise ArgumentError, 'Erreur lors du login'
      end
    else
      raise ArgumentError, 'Erreur lors du login'
    end
  end

  def self.validate_email email
    email =~ /\A[^@\s]+@[^@\s]+\z/ ? true : false
  end

  def email_must_be_valid
    errors.add('Format d\'email non valide') unless User.validate_email(email)
  end
end
