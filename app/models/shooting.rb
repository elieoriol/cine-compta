class Shooting < ApplicationRecord
  has_many :weeks, dependent: :destroy
  has_many :days, through: :weeks
  has_many :hours, through: :days
  has_many :employees, dependent: :destroy

  has_many :indemnities, dependent: :destroy

  belongs_to :user, optional: false

  validates_presence_of :name

  def number_of_weeks
    weeks.nil? ? 0 : weeks.count
  end
end
