class ShootingsController < ApplicationController
  before_action :check_for_user_session

  def index
    @shootings = current_user.shootings
    @current_shooting = @shootings.nil? ? nil : @shootings.order(updated_at: :desc).first
    @new_shooting = current_user.shootings.new
    # All your shootings (archive page also later ?) with number of weeks etc
    # Create a new shooting from here
  end

  def show
    # All the employees, manage the contracts from here
    # See also the weeks
    # Maybe split the two in two pages
    @shooting = current_user.shootings.find(params[:id])
    session[:shooting_id] = @shooting.id
    @employees = @shooting.employees
    @indemnities = @shooting.indemnities
    @last_weeks = @shooting.weeks.order(beginning_date: :desc).take(4)
    @new_week = @shooting.weeks.new
    @new_employee = @shooting.employees.new
    @new_indemnity = @shooting.indemnities.new
  end

  def get_week
    shooting = current_user.shootings.find(params[:shooting_id])
    beginning_date = Date.strptime(params[:date], "%d/%m/%Y").beginning_of_week
    week = shooting.weeks.find_by(beginning_date: beginning_date)
    return redirect_to shooting, flash:{ danger: "Cette semaine n'existe pas" } if week.nil?
    redirect_to week
  end

  def create_employee
    shooting = current_user.shootings.find(params[:shooting_id])

    contract_start = params[:employee][:contract_start].empty? ? nil : Date.strptime(params[:employee][:contract_start], '%d/%m/%Y')
    contract_end = params[:employee][:contract_end].empty? ? nil : Date.strptime(params[:employee][:contract_end], '%d/%m/%Y')

    employee = shooting.employees.new(name: params[:employee][:name],
                                      job_id: params[:employee][:job_id],
                                      contract_start: contract_start,
                                      contract_end: contract_end,
                                      daily_contract: params[:employee][:daily_contract])

    if employee.save
      redirect_to shooting, flash: { success: 'Salarié ajouté' }
    else
      redirect_to shooting, flash: { danger: "Echec de l'ajout du salarié" }
    end
  end

  def update_employee
    shooting = current_user.shootings.find(params[:shooting_id])
    employee = shooting.employees.find(params[:employee_id])

    contract_start = params[:employee][:contract_start].empty? ? nil : Date.strptime(params[:employee][:contract_start], '%d/%m/%Y')
    contract_end = params[:employee][:contract_end].empty? ? nil : Date.strptime(params[:employee][:contract_end], '%d/%m/%Y')

    employee.update_attributes(name: params[:employee][:name],
                               job_id: params[:employee][:job_id],
                               contract_start: contract_start,
                               contract_end: contract_end,
                               daily_contract: params[:employee][:daily_contract])

    if employee.save
      redirect_to shooting, flash: { success: 'Salarié mis à jour' }
    else
      redirect_to shooting, flash: { danger: "Echec de la mise à jour du salarié" }
    end
  end

  def delete_employee
    shooting = current_user.shootings.find(params[:shooting_id])
    employee = shooting.employees.find(params[:employee_id]).destroy

    if employee.destroyed?
      redirect_to shooting, flash: { success: 'Salarié mis à jour' }
    else
      redirect_to shooting, flash: { danger: "Echec de la mise à jour du salarié" }
    end
  end

  def employee_contract_end
    shooting = current_user.shootings.find(params[:shooting_id])
    employee = shooting.employees.find(params[:employee_id])
    employee.update_attribute(:contract_end, Date.today)

    redirect_to shooting, flash: { success: 'Fin du contrat ajoutée pour ' + employee.name }
  end

  def create_indemnity
    shooting = current_user.shootings.find(params[:shooting_id])

    indemnity = shooting.indemnities.new(name: params[:indemnity][:name],
                                         value: params[:indemnity][:value])

    if indemnity.save
      redirect_to shooting, flash: { success: 'Indemnité ajoutée' }
    else
      redirect_to shooting, flash: { danger: "Echec de l'ajout de l'indemnité" }
    end
  end

  def update_indemnity
    shooting = current_user.shootings.find(params[:shooting_id])
    indemnity = shooting.indemnities.find(params[:indemnity_id])

    indemnity.update_attributes(name: params[:indemnity][:name],
                                value: params[:indemnity][:value])

    if indemnity.save
      redirect_to shooting, flash: { success: 'Indemnité mise à jour' }
    else
      redirect_to shooting, flash: { danger: "Echec de la mise à jour de l'indemnité" }
    end
  end

  def delete_indemnity
    shooting = current_user.shootings.find(params[:shooting_id])
    indemnity = shooting.indemnities.find(params[:indemnity_id]).destroy

    if indemnity.destroyed?
      redirect_to shooting, flash: { success: 'Indemnité supprimée' }
    else
      redirect_to shooting, flash: { danger: "Echec de la suppression de l'indemnité" }
    end
  end

  def create
    shooting = current_user.shootings.new(name: params[:shooting][:name], start: params[:shooting][:start])
    if shooting.save
      redirect_to shooting, flash:{ success: 'Tournage créé' }
    else
      redirect_to shootings_path, flash:{ danger: 'Echec de la création du tournage' }
    end
  end

  def update
    shooting = current_user.shootings.find(params[:id])
    if shooting.update_attribute(:name, params[:shooting][:name])
      redirect_to shooting, flash:{ success: 'Tournage mis à jour' }
    else
      redirect_to shootings_path, flash:{ danger: 'Echec de la mise à jour du tournage' }
    end
  end

  def destroy
    begin
      current_user.shootings.find(params[:id]).destroy
      redirect_to shootings_path, flash:{ success: 'Tournage supprimé' }
    rescue Exception => e
      redirect_to shootings_path, flash:{ danger: e.message }
    end
  end
end
