class WeeksController < ApplicationController
  before_action :check_for_user_session
  before_action :check_for_shooting_session

  def show
    @shooting = current_shooting
    @week = Week.find(params[:id])
    session[:week_id] = @week.id
    @employees = @shooting.employees
    # Show current week
    # Possibility to create/edit a day of the current week
    # Mix the days of employees
  end

  def create
    shooting = current_shooting
    beginning_date = Date.strptime(params[:week][:beginning_date], "%d/%m/%Y").beginning_of_week
    return redirect_to shooting, flash:{ danger: 'Cette semaine existe déjà' } if shooting.weeks.find_by(beginning_date: beginning_date)

    week = shooting.weeks.new(beginning_date: beginning_date)
    if week.save
      redirect_to week, flash:{ success: 'Semaine créée' }
    else
      redirect_to shooting, flash:{ danger: 'Echec de la création de semaine' }
    end
  end

  def employee_selection
    shooting = current_shooting
    week = shooting.weeks.find(params[:week_id])
    @employee = shooting.employees.find(params[:employee_id])
    @days = shooting.days.where(employee: @employee, week: week)
    @indemnities = shooting.indemnities

    render 'weeks/employee_selection'
  end

  def create_day
    date = Date.parse(params[:date].to_s)
    day = Day.create(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], date: date)

    start_date = DateTime.parse(params[:date])
    end_date = DateTime.parse(params[:date]) + 3.hours

    start_hour = datetime_to_hour(start_date)
    end_hour = datetime_to_hour(end_date)
    end_hour += (Date.parse(end_date.to_s) - Date.parse(start_date.to_s)).to_i * 24

    day.update_attributes(start_hour: start_hour, end_hour: end_hour)

    render json: {event: day}
  end

  def update_day
    date = Date.parse(params[:start].to_s)
    day = Day.find_by(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], date: date, id: params[:id])

    if day.nil?
      day = Day.create(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], date: date)
      Day.find(params[:id]).destroy
    end

    start_date = DateTime.parse(params[:start])
    end_date = DateTime.parse(params[:end])

    start_hour = datetime_to_hour(start_date)
    end_hour = datetime_to_hour(end_date)
    end_hour += (Date.parse(params[:end]) - Date.parse(params[:start])).to_i * 24

    day.update_attributes(start_hour: start_hour, end_hour: end_hour)

    render json: {event: day}
  end

  def delete_day
    day = Day.find_by(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], id: params[:id])
    day.destroy

    render json: {success: day.destroyed?}
  end

  def add_lunch
    day = Day.find_by(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], id: params[:id])
    day.update_attributes(lunch_hour: day.start_hour)

    render json: {event: day.as_json({lunch: true})}
  end

  def update_lunch
    day_id = params[:id][0..-7]
    pp day_id
    day = Day.find_by(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], id: day_id)
    lunch_hour = datetime_to_hour(DateTime.parse(params[:start]))
    day.update_attributes(lunch_hour: lunch_hour)

    render json: {event: day.as_json({lunch: true})}
  end

  def remove_lunch
    day_id = params[:id][0..-7]
    day = Day.find_by(shooting: current_shooting, employee_id: params[:employee_id], week_id: params[:week_id], id: day_id)
    day.update_attributes(lunch_hour: nil)

    render json: {success: day.lunch_hour.nil?}
  end

  def get_shooting_hours
    week = current_shooting.weeks.find(params[:week_id])

    render json: {shooting_hours: week.shooting_hours}
  end

  def update_shooting_hours
    week = current_shooting.weeks.find(params[:week_id])
    week.update_attribute(:shooting_hours, params[:shooting_hours])

    redirect_to week unless week.updated_at_changed?
  end

  private
  def datetime_to_hour(date)
    time = Time.parse(date.to_s)
    time.hour + time.min/60.0
  end
end
