class UsersController < ApplicationController
  before_action :redirect_if_session, only:[:get_login, :get_signup, :do_login, :do_signup]

  before_action :check_for_user_session, except:[:get_login, :get_signup, :do_signup, :do_login, :do_logout]

  def data
    # Maybe change the name of this action
    # Manage the jobs and all that kind of constant stuff
    @jobs = Job.all
  end

  def get_signup
    @user = User.new
    render :signup, layout: 'garden'
  end

  def do_signup
    begin
      user = User.new(user_params)
      if user.save
        log_in user
        redirect_to shootings_path, flash:{ success: 'Enregistrement réussi'}
      else
        raise ArgumentError, user.errors.full_messages.join(' ; ')
      end
    rescue Exception => e
      redirect_to :render_signup, flash:{ danger: e.message }
    end
  end

  def get_login
    render :login, layout: 'garden'
  end

  def do_login
    begin
      user = User.login params[:user][:email], params[:user][:password]
      log_in user
      redirect_to shootings_path
    rescue Exception => e
      redirect_to :render_login, flash:{ danger: (e.message)}
    end
  end

  def do_logout
    log_out
    redirect_to :root, flash: {success: 'Déconnexion réussie'}
  end


  private

  def redirect_if_session
    if session[:user_id]
      redirect_to shootings_path
    end
  end

  def user_params
    params.require(:user).permit(:email,
                                 :password,
                                 :password_confirmation)
  end
end
