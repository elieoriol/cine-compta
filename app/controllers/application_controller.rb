class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception


  # User session helpers
  def current_user
    return nil unless session[:user_id]
    @current_user ||= User.find(session[:user_id])
    raise ArgumentError, 'session_error' unless @current_user
    @current_user
  end

  def check_for_user_session
    begin
      current_user
    rescue
      redirect_to :render_login_path
    end
  end

  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
  end

  # Logs out the current user.
  def log_out
    session.delete(:user_id)
    session.delete(:shooting_id)
    session.delete(:week_id)
    @current_user = nil
    @current_shooting = nil
    @current_week = nil
  end

  # Shooting session helpers
  def current_shooting
    return nil unless session[:shooting_id]
    @current_shooting ||= Shooting.find(session[:shooting_id])
    raise ArgumentError, 'session_error' unless @current_shooting
    @current_shooting
  end

  def check_for_shooting_session
    begin
      current_shooting
    rescue
      redirect_to shootings_path
    end
  end

  # Shooting session helpers
  def current_week
    return nil unless session[:week_id]
    @current_week ||= Week.find(session[:week_id])
    raise ArgumentError, 'session_error' unless @current_week
    @current_week
  end
end
