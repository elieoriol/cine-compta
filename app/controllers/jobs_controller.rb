class JobsController < ApplicationController
  def index
    @jobs = Job.all.order(name: :asc)
    @new_job = Job.new
  end

  def create
    job = Job.new(job_params)
    if job.save
      redirect_to jobs_path, flash:{ success: 'Fonction créée' }
    else
      redirect_to jobs_path, flash:{ danger: 'Echec de la création de la fonction' }
    end
  end

  def update
    job = Job.find(params[:id])
    if job.update_attributes(job_params)
      redirect_to jobs_path, flash:{ success: 'Fonction mise à jour' }
    else
      redirect_to jobs_path, flash:{ danger: 'Echec de la mise à jour de la fonction' }
    end
  end

  def destroy
    begin
      Job.find(params[:id]).destroy
      redirect_to jobs_path, flash:{ success: 'Fonction supprimée' }
    rescue Exception => e
      redirect_to jobs_path, flash:{ danger: e.message }
    end
  end
end

private

def job_params
  params.require(:job).permit(:name,
                              :base_salary,
                              :equiv_from,
                              :equiv_to)
end