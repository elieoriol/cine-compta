Rails.application.routes.draw do
  get 'jobs/index'

  get '/signup', to: 'users#get_signup', as: :render_signup
  post '/signup', to: 'users#do_signup', as: :signup

  root to: 'users#get_login'
  get '/login', to: 'users#get_login', as: :render_login
  post '/login', to: 'users#do_login', as: :login

  get '/logout', to: 'users#do_logout', as: :logout


  resources :shootings, only: [:index, :show, :create, :update, :destroy]
  post '/shootings/:shooting_id/get_week', to: 'shootings#get_week', as: :get_week

  post '/shootings/:shooting_id/create_employee', to: 'shootings#create_employee', as: :create_employee
  patch '/shootings/:shooting_id/update_employee/:employee_id', to: 'shootings#update_employee', as: :update_employee
  post '/shootings/:shooting_id/delete_employee/:employee_id', to: 'shootings#delete_employee', as: :delete_employee
  post '/shootings/:shooting_id/employee_contract_end/:employee_id', to: 'shootings#employee_contract_end', as: :employee_contract_end

  post '/shootings/:shooting_id/create_indemnity', to: 'shootings#create_indemnity', as: :create_indemnity
  patch '/shootings/:shooting_id/update_indemnity/:indemnity_id', to: 'shootings#update_indemnity', as: :update_indemnity
  post '/shootings/:shooting_id/delete_indemnity/:indemnity_id', to: 'shootings#delete_indemnity', as: :delete_indemnity

  resources :weeks, only: [:show, :create]
  post '/weeks/:week_id/employee_selection', to: 'weeks#employee_selection', as: :employee_selection
  post '/weeks/:week_id/update_day', to: 'weeks#update_day', as: :update_day
  post '/weeks/:week_id/create_day', to: 'weeks#create_day', as: :create_day
  post '/weeks/:week_id/delete_day', to: 'weeks#delete_day', as: :delete_day
  post '/weeks/:week_id/add_lunch', to: 'weeks#add_lunch', as: :add_lunch
  post '/weeks/:week_id/update_lunch', to: 'weeks#update_lunch', as: :update_lunch
  post '/weeks/:week_id/remove_lunch', to: 'weeks#remove_lunch', as: :remove_lunch
  post '/weeks/:week_id/get_shooting_hours', to: 'weeks#get_shooting_hours', as: :get_shooting_hours
  post '/weeks/:week_id/update_shooting_hours', to: 'weeks#update_shooting_hours', as: :update_shooting_hours

  resources :jobs, only: [:index, :create, :update, :destroy]
end
