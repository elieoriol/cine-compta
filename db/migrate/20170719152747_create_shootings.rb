class CreateShootings < ActiveRecord::Migration[5.1]
  def change
    create_table :shootings, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.string :name, null: false
      t.uuid :user_id, null: false
      t.date :start
      t.date :end

      t.timestamps
    end
  end
end
