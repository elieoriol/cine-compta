class AddIndexes < ActiveRecord::Migration[5.1]
  def change

    add_index :users, :email
    add_index :employees, :job_id
    add_index :employees, :shooting_id
    add_index :days, :date
    add_index :days, :employee_id
    add_index :days, :week_id
    add_index :days, :shooting_id
    add_index :hours, :day_id
    add_index :shootings, :user_id
    add_index :weeks, :shooting_id
    add_index :weeks, :beginning_date
    add_index :indemnities, :shooting_id
  end
end
