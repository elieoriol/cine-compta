class CreateIndemnities < ActiveRecord::Migration[5.1]
  def change
    create_table :indemnities, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.uuid :shooting_id, null: false

      t.string :name, null: false
      t.float :value, null: false

      t.timestamps
    end
  end
end
