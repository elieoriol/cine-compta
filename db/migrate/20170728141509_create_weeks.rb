class CreateWeeks < ActiveRecord::Migration[5.1]
  def change
    create_table :weeks, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.date :beginning_date, null: false
      t.uuid :shooting_id, null: false

      t.jsonb :shooting_hours, default: {
          '1': {'start': nil, 'end': nil},
          '2': {'start': nil, 'end': nil},
          '3': {'start': nil, 'end': nil},
          '4': {'start': nil, 'end': nil},
          '5': {'start': nil, 'end': nil},
          '6': {'start': nil, 'end': nil},
          '7': {'start': nil, 'end': nil},
      }

      t.timestamps
    end
  end
end
