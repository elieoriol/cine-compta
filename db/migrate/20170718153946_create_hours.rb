class CreateHours < ActiveRecord::Migration[5.1]
  def change
    create_table :hours, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.uuid :day_id, null: false

      t.float :day_hour
      t.float :week_num
      t.float :day_num

      t.timestamps
    end
  end
end
