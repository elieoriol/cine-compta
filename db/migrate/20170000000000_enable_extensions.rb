#This enables pgcrypto, which is used for uuid's
class EnableExtensions < ActiveRecord::Migration[5.1]
  def change
    enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')
  end
end