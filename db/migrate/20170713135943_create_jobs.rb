class CreateJobs < ActiveRecord::Migration[5.1]
  def change
    create_table :jobs, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.string :name, null: false
      t.float :base_salary, null: false
      t.integer :equiv_to, null: false
      t.integer :equiv_from, null: false

      t.timestamps
    end
  end
end
