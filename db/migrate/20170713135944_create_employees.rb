class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.uuid :job_id, null: false
      t.uuid :shooting_id, null: false
      t.string :name, null: false

      # Contract_info
      t.date :contract_start
      t.date :contract_end
      t.boolean :daily_contract

      t.timestamps
    end
  end
end
