class CreateDays < ActiveRecord::Migration[5.1]
  def change
    create_table :days, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.uuid :employee_id, null: false
      t.uuid :week_id, null: false
      t.uuid :shooting_id, null: false

      t.date :date, null: false

      t.float :start_hour
      t.float :end_hour
      t.float :lunch_hour
      t.float :travelling_time, default: 0

      t.timestamps
    end
  end
end
