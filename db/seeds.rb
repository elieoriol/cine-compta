# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user = User.create(email: 'ex@ex.com', password: '123123', password_confirmation: '123123')

shooting = user.shootings.create(name: 'Yoyoyoyo YO')

prod = Job.create(name: 'Producteur', base_salary: 1500.00, equiv_to: 35, equiv_from: 38)
brico = Job.create(name: 'Bricoleur', base_salary: 456.23, equiv_to: 41, equiv_from: 45)

fred = shooting.employees.create(job: prod, name: 'Frédéric')
dani = shooting.employees.create(job: brico, name: 'Daniel')
shooting.employees.create(job: brico, name: 'Michel')

current_week = shooting.weeks.create(beginning_date: Date.today.beginning_of_week)

current_week.days.create(employee: fred,
                         shooting: shooting,
                         date: Date.today,
                         start_hour: 9.0,
                         end_hour: 16.0,
                         lunch_hour: 12.0)

current_week.days.create(employee: dani,
                         shooting: shooting,
                         date: Date.today,
                         start_hour: 7.0,
                         end_hour: 18.0,
                         lunch_hour: 12.0)