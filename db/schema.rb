# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180000000000) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pgcrypto"

  create_table "days", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "employee_id", null: false
    t.uuid "week_id", null: false
    t.uuid "shooting_id", null: false
    t.date "date", null: false
    t.float "start_hour"
    t.float "end_hour"
    t.float "lunch_hour"
    t.float "travelling_time", default: 0.0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["date"], name: "index_days_on_date"
    t.index ["employee_id"], name: "index_days_on_employee_id"
    t.index ["shooting_id"], name: "index_days_on_shooting_id"
    t.index ["week_id"], name: "index_days_on_week_id"
  end

  create_table "employees", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "job_id", null: false
    t.uuid "shooting_id", null: false
    t.string "name", null: false
    t.date "contract_start"
    t.date "contract_end"
    t.boolean "daily_contract"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["job_id"], name: "index_employees_on_job_id"
    t.index ["shooting_id"], name: "index_employees_on_shooting_id"
  end

  create_table "hours", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "day_id", null: false
    t.float "day_hour"
    t.float "week_num"
    t.float "day_num"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["day_id"], name: "index_hours_on_day_id"
  end

  create_table "indemnities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "shooting_id", null: false
    t.string "name", null: false
    t.float "value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shooting_id"], name: "index_indemnities_on_shooting_id"
  end

  create_table "jobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.float "base_salary", null: false
    t.integer "equiv_to", null: false
    t.integer "equiv_from", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shootings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.uuid "user_id", null: false
    t.date "start"
    t.date "end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_shootings_on_user_id"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", null: false
    t.string "password_digest", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email"
  end

  create_table "weeks", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.date "beginning_date", null: false
    t.uuid "shooting_id", null: false
    t.jsonb "shooting_hours", default: {"1"=>{"end"=>nil, "start"=>nil}, "2"=>{"end"=>nil, "start"=>nil}, "3"=>{"end"=>nil, "start"=>nil}, "4"=>{"end"=>nil, "start"=>nil}, "5"=>{"end"=>nil, "start"=>nil}, "6"=>{"end"=>nil, "start"=>nil}, "7"=>{"end"=>nil, "start"=>nil}}
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["beginning_date"], name: "index_weeks_on_beginning_date"
    t.index ["shooting_id"], name: "index_weeks_on_shooting_id"
  end

end
